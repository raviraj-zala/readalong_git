<?php

namespace App\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;

class RandomHelper extends Controller
{
	public static function randomKey($length) {
	    $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));
	    $key = "";
	    for($i=0; $i < $length; $i++) {
	        $key .= $pool[mt_rand(0, count($pool) - 1)];
	    }
	    return $key;
	}
}