<?php

namespace App\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Exceptions;
use App\User;

class UserHelper extends Controller
{
    public static function getUserShareable($id)
    {
    	try {
    		if(User::where('id',$id)->exists()){
	    		$userInfo = User::where('id',$id)->get()->first()->shareable_id;
	    		return $userInfo;
    		}else{
    			return "UserNotFound";
    		}
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }
}
