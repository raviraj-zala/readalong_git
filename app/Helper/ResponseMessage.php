<?php 
namespace App\Helper;

use Illuminate\Database\Eloquent\Helper;

/**
* 
*/
class ResponseMessage
{
	public static function success($msg,$data = array())
	{
		echo json_encode(['status' => true, 'error' => 200, 'message' => $msg, 'data' => $data]);
		exit;
	}

	public static function error($msg)
	{
		echo json_encode(['status' => false, 'error' => 401, 'message' => $msg]);	
		exit;
	}
}
?>