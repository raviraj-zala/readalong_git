<?php

namespace App\Http\Controllers\RESTAPIs\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;

class CategoryserviceController extends Controller
{
    public function get_category()
    {
    	try {
			$category = Category::where('status','1')->select('name')->get();
			ResponseMessage::success("Category List",$category);
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }
}
