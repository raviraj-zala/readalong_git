<?php

namespace App\Http\Controllers\RESTAPIs\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use App\Helper\RandomHelper;
use Validator;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\Helper\UserHelper;
use App\Model\Video;
use App\Model\Favourite;
use File;
use App\Model\Views;
use DB;

class VideoserviceController extends Controller
{
    public function upload_video(Request $request)
    {
    	try 
    	{
    		$rules = [
				'video' => '',
				'user_id' => 'required',
				'title' => 'required',
				'description' => '',
				'who_see' => '',
				'age' => 'required',
				'find_by' => 'required',
				'learning_profile' => 'required',
				'file_type' => 'required',
				'images[]' => ''
			];
			$customeMessage = [
				'video.required' => 'Please send video.',
				'user_id.required' => 'Please enter user id.',
				'title.required' => 'Please enter title.',
				'age.required' => 'Please enter age.',
				'find_by.required' => 'Please enter find by.',
				'learning_profile.required' => 'Please enter learning profile.',
			];
			$validator = Validator::make($request->all(),$rules,$customeMessage);
	        if($validator->fails()){
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        }else{
	        	$video = trim($request->video);
	        	$user_id = trim($request->user_id);
	        	$title = trim($request->title);
	        	$description = trim($request->description);
	        	$age = trim($request->age);
	        	$find_by = trim($request->find_by);
	        	$learning_profile = trim($request->learning_profile);
                $shareable_id = UserHelper::getUserShareable($user_id);

                $images = $request->file('images');
                $picture = array();
                $counter = 0;
                $unique_id = "POST".time();

	        	if($request->hasFile('video'))
                {
                	$file = $request->video;
                	$mime = $file->getMimeType();
                	if($mime == "video/x-flv" || $mime == "video/mp4" || $mime == "video/3gpp" || $mime == "video/quicktime" || $mime == "video/x-msvideo" || $mime == "video/x-ms-wmv" || $mime == "video/webm" || $mime == "video/ogg" || $mime == "video/x-m4v" )
                	{
	                	if($shareable_id==="UserNotFound"){
	                		ResponseMessage::error('User not found.');
	                	}else{
		                    $file = $request->video;
		                    $time = md5(time());
		                    $video_name = $time.'.'.$file->getClientOriginalExtension();
	                		$path = 'user/'.$shareable_id.'/videos/'.$video_name;
		                    $file->move(public_path('user/'.$shareable_id.'/videos/'),$video_name);

	                		if(!File::exists(public_path('user/'.$shareable_id.'/videos/thumbnail/'))) {
							    File::makeDirectory(public_path('user/'.$shareable_id.'/videos/thumbnail/'), 0777, true, true);
							}
		                    $video_path = public_path($path);
	                		$thumbnail_image  = $time.".png";
	                		$thumbnail_path = public_path('user/'.$shareable_id.'/videos/thumbnail/').$thumbnail_image;
	                		$thumb_path = 'user/'.$shareable_id.'/videos/thumbnail/'.$thumbnail_image;;

	                		$temp = shell_exec("ffmpeg -i $video_path -deinterlace -an -ss 1 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $thumbnail_path 2>&1");

	                		$regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
							if (preg_match($regex_duration, $temp, $regs)) 
							{
							$hours = $regs [1] ? $regs [1] : null;
							$mins = $regs [2] ? $regs [2] : null;
							$secs = $regs [3] ? $regs [3] : null;
							}
							$video_Length = $hours . ":" . $mins . ":" . $secs;

		                    $video = New Video;
		                    $video->user_id = $user_id;
		                    $video->title = $title;
		                    $video->description = $description;
		                    $video->path = $path;
		                    $video->thumb_path = $thumb_path;
		                    $video->Duration = $video_Length;
		                    $video->age = $age;
		                    $video->find_by = $find_by;
		                    $video->file_type = "video";
		                    $video->unique_id = $unique_id;
		                    $video->learning_profile = $learning_profile;
		                    if($video->save()){
		                    	ResponseMessage::success('Video successfully upload.',$video);
		                    }else{
		                    	ResponseMessage::error('Somwthig was wrong in video upload please try again.');	
		                    }

	                	}
                	} else{
                		ResponseMessage::error('Invalid video formate.');
            		}
                }else if($images){
     				foreach($images as $file){
		            	$time = md5(time());
				        $extension = $file->getClientOriginalExtension();
				        $image_name = $counter.$time.'.'.$file->getClientOriginalExtension();
				        $file->move(public_path('user/'.$shareable_id.'/images/'),$image_name);
				        $filepath = 'user/'.$shareable_id.'/images/'.$image_name;
				        $counter++;

				        $video = New Video;
	                    $video->user_id = $user_id;
	                    $video->title = $title;
	                    $video->description = $description;
	                    $video->path = $filepath;
	                    $video->thumb_path = "";
	                    $video->Duration = "";
	                    $video->age = $age;
	                    $video->find_by = $find_by;
	                    $video->file_type = "image";
	                    $video->unique_id = $unique_id;
	                    $video->learning_profile = $learning_profile;
	                    if($video->save()){
	                    }else{
	                    	ResponseMessage::error('Somwthig was wrong in video upload please try again.');	
	                    }
				    }
				    ResponseMessage::success('Image successfully upload.',"");
            	} else{
                	ResponseMessage::error('Somwthig was wrong in video upload please try again.');	
                }
	        }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function get_all_videos(Request $request)
    {
    	try {
    		$rules = [
				'user_id' => 'required',
			];
			$customeMessage = [
				'user_id.required' => 'Please enter user id.'
			];
			$validator = Validator::make($request->all(),$rules,$customeMessage);
	        if($validator->fails()){
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        }else{
	        	$id = $request->user_id;
				$videoList = Video::leftjoin('users','users.id','=','videos.user_id')	
					->leftjoin('favourites',function($join) use($id){
						$join->on('favourites.video_id','=','videos.id')->where('favourites.user_id','=',$id);
					})
					->select('videos.*','users.username as upload_by','users.profile','favourites.is_favourite'
						)
					->orderBy('videos.id','DESC')
					->get();

				$temp = array();
				$var = "";
				$counter = 1;
				$tempcounter = 0;

				$newTemp = array();
				foreach ($videoList as $video) 
				{
					if($var == "" || $var != $video->unique_id)
					{
						$tempcounter = 1;
						$var = $video->unique_id;
						$newTemp[$counter][$tempcounter] = $video;
						$counter++;
					}else{
						$tempcounter++;
						$newTemp[$counter][$tempcounter] = $video;
					}
				}
				// return response()->json($newTemp);
				// return response()->json($temp);
				ResponseMessage::success('Video List.',$newTemp);
			}	
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function get_single_video(Request $request)
    {
    	try {
    		$rules = [
				'user_id' => 'required',
			];
			$customeMessage = [
				'user_id.required' => 'Please enter user id.'
			];
			$validator = Validator::make($request->all(),$rules,$customeMessage);
	        if($validator->fails()){
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        }else{
	        	$user_id = $request->user_id;
	        	$unique_id = $request->unique_id;

	        	if(Views::where('video_unique_id',$unique_id)->where('user_id',$user_id)->exists()){
	        	} else{
	        		$views = New Views;
	        		$views->video_unique_id = $unique_id;
	        		$views->user_id = $user_id;
	        		$views->save();
	        	}
				$fav_count['fav_count'] = Favourite::where('video_id',$unique_id)->count();
				$fav_count['view_count'] = Views::where('video_unique_id',$unique_id)->count();
	        	ResponseMessage::success('Video Count.',$fav_count);
	        }
    	} catch (Exception $e) {
    		
    	}
    }

    public function delete_video($id)
    {
    	try {
    		if(Video::where('id',$id)->exists()){
    			$videoInfo = Video::find($id)->delete();
				ResponseMessage::success('Video successfully delete.',$videoInfo);
    		}else{
    			ResponseMessage::error('Video is not exists please try again.');	
    		}
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function add_favourite(Request $request)
    {
    	try 
    	{
    		$rules = [
				'video_id' => 'required',
				'user_id' => 'required',
			];
			$customeMessage = [
				'video_id.required' => 'Please send video id.',
				'user_id.required' => 'Please enter user id.'
			];
			$validator = Validator::make($request->all(),$rules,$customeMessage);
	        if($validator->fails()){
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        }else{
	        	$video_id = $request->video_id;
	        	$user_id = $request->user_id;
	        	if(User::where('id',$user_id)->exists())
	        	{
	        		if(Favourite::where('user_id',$user_id)->where('video_id',$video_id)->exists()){
	        			$favourite = Favourite::where('user_id',$user_id)->where('video_id',$video_id)->delete();
	        			if($favourite){
	        				ResponseMessage::success('Remove from favourite.',['favourite_status' => '0']);	
	        			}else{
	        				ResponseMessage::error('Something is wrong. please try again.');
	        			}
	        		}else{
		        		$favourite = New Favourite;
		        		$favourite->user_id = $user_id;
		        		$favourite->video_id = $video_id;
		        		$favourite->created_at = date('Y-m-d H:i:s');
		        		if($favourite->save())
		        		{
		        			ResponseMessage::success('Video added favourite.',['favourite_status' => '1']);
		        		}else{
		        			ResponseMessage::error('Something is wrong. please try again.');	
		        		}
	        		}
	        	}else{
	        		ResponseMessage::error('User is not exists please try again.');
	        	}
	        }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

}
