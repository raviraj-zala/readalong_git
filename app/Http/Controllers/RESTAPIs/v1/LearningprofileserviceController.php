<?php

namespace App\Http\Controllers\RESTAPIs\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Learningprofile;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;

class LearningprofileserviceController extends Controller
{
    public function get_learningprofile()
    {
    	try {
			$learningprofile = Learningprofile::where('status','1')->select('name')->get();
			ResponseMessage::success("Learning Profile List",$learningprofile);
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }
}
