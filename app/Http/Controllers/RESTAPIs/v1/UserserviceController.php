<?php

namespace App\Http\Controllers\RESTAPIs\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use App\Helper\RandomHelper;
use Validator;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\Helper\UserHelper;
use App\Model\Video;
use File;

class UserserviceController extends Controller
{
    public function signup(Request $request)
    {
    	try 
    	{
	    	$rules = [
				'name' => 'required',
				'email' => 'required|email|max:255|unique:users',
				'username' => '',
				'password' => '',
				'profile' => '',
				'device_type' => 'required|in:Iphone,Android,Other',
				'device_token' => '',
				'fb_app_id' => '',
				'gmail_app_id' => '',
				'register_type' => '',
				'mobile_no' => '',
			];
			$customeMessage = [
				'name.required' => 'Please enter name',
				'email.email' => 'Please enter valid email id.',
				'email.unique' => 'Email is already exists.',
				'device_type.required' => 'Please enter device type',
				'device_type.in' => 'Device type must be Iphone,Android and Other',
			];
			$validator = Validator::make($request->all(),$rules,$customeMessage);
	        if($validator->fails()){
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        }else{
	        	$randomString = RandomHelper::randomKey(10);
	        	$name = trim($request->name);
	        	$email = trim($request->email);
	        	$username = trim($request->username);
	        	$password = Hash::make(trim($request->password));
	        	$profile = trim($request->profile);
	        	$device_type = trim($request->device_type);
	        	$device_token = trim($request->device_token);
	        	$fb_app_id = trim($request->fb_app_id);
	        	$gmail_app_id = trim($request->gmail_app_id);
	        	$status = "1";
	        	$register_type = trim($request->register_type);
	        	$mobile_no = trim($request->mobile_no);

	        	if($register_type=="Facebook"){
	        		$newUser = New User;
		        	$newUser->name = $name;
		        	$newUser->shareable_id = $randomString;
		        	$newUser->email = $email;
		        	$newUser->username = $username;
		        	$newUser->password = $password;
		        	$newUser->profile = $profile;
		        	$newUser->device_type = $device_type;
		        	$newUser->device_token = $device_token;
		        	$newUser->fb_app_id = $fb_app_id;
		        	$newUser->gmail_app_id = $gmail_app_id;
		        	$newUser->status = $status;
		        	$newUser->register_type = $register_type;
		        	if($newUser->save()){
		        		ResponseMessage::success('Successfully register.',$newUser);
		        	}else{
		        		ResponseMessage::error('Somethig was wrong please try again.');
		        	}
	        	}elseif($register_type=="Gmail"){
	        		$newUser = New User;
		        	$newUser->name = $name;
		        	$newUser->shareable_id = $randomString;
		        	$newUser->email = $email;
		        	$newUser->username = $username;
		        	$newUser->password = $password;
		        	$newUser->profile = $profile;
		        	$newUser->device_type = $device_type;
		        	$newUser->device_token = $device_token;
		        	$newUser->fb_app_id = $fb_app_id;
		        	$newUser->gmail_app_id = $gmail_app_id;
		        	$newUser->status = $status;
		        	$newUser->register_type = $register_type;
		        	if($newUser->save()){
		        		ResponseMessage::success('Successfully register.',$newUser);
		        	}else{
		        		ResponseMessage::error('Somethig was wrong please try again.');
		        	}
	        	}elseif($register_type=="Basic"){
	        		if($request->profile!=""){
		        		if(!File::exists(public_path('user/'.$randomString.'/profile/'))) {
						    File::makeDirectory(public_path('user/'.$randomString.'/profile/'), 0777, true, true);
						}

		        		$time = md5(time());
		                $img_url = $time.'.'.$request['profile']->getClientOriginalExtension();
		                $img_ext = $request['profile']->getClientOriginalExtension();
		                $request['profile']->move(public_path('user/'.$randomString.'/profile/'), $img_url);
		        		$profile = 'user/'.$randomString.'/profile/'.$img_url ;
		        	}else{
		        		$profile = "";
		        	}
		        	$newUser = New User;
		        	$newUser->name = $name;
		        	$newUser->shareable_id = $randomString;
		        	$newUser->email = $email;
		        	$newUser->username = $username;
		        	$newUser->password = $password;
		        	$newUser->mobile_no = $mobile_no;
		        	$newUser->profile = $profile;
		        	$newUser->device_type = $device_type;
		        	$newUser->device_token = $device_token;
		        	$newUser->fb_app_id = $fb_app_id;
		        	$newUser->gmail_app_id = $gmail_app_id;
		        	$newUser->status = $status;
		        	$newUser->register_type = $register_type;
		        	if($newUser->save()){
		        		ResponseMessage::success('Successfully register.',$newUser);
		        	}else{
		        		ResponseMessage::error('Somethig was wrong please try again.');
		        	}
	        	}else{
	        		ResponseMessage::error('Register type invalid.');
	        	}
	        }
        } catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function login(Request $request)
    {
    	try {
    		$rules = [
				'email' => '',
				'password' => '',
				'device_type' => 'required|in:Iphone,Android,Other',
				'device_token' => 'required',
				'fb_app_id' => '',
				'gmail_app_id' => ''
			];
			$customeMessage = [
				'device_type.required' => 'Please enter device type',
				'device_type.in' => 'Device type must be Iphone,Android and Other',
				'device_token.required' => 'Please enter device token.'
			];
			$validator = Validator::make($request->all(),$rules,$customeMessage);
	        if($validator->fails()){
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        }else{
	        	$email = trim($request->email);
	        	$password = trim($request->password);
	        	$device_type = trim($request->device_type);
	        	$device_token = $request->device_token;
	        	$fb_app_id = trim($request->fb_app_id);
	        	$gmail_app_id = trim($request->gmail_app_id);
	        	
	        	if($fb_app_id!=""){

	        	}elseif($gmail_app_id!=""){

	        	}elseif($email!=""){
					$user = User::where('email',$email)->get()->first();
					if($user){
						if(Hash::check($password,$user->password)){
							$userUpdate = User::find($user->id);
							$userUpdate->device_token = $device_token;
							if($userUpdate->save())
							{
								ResponseMessage::success('Login successfully.',$userUpdate);
							}
							else
							{
								ResponseMessage::error('Token not update.');
							}
						}else{
							ResponseMessage::error('Password not match.');
						}
					}else{
						ResponseMessage::error('Email not registered.');
					}
	        	}else{
	        		ResponseMessage::error('Detail is insufficient.');
	        	}

	        }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function upload_video(Request $request)
    {
    	try 
    	{
    		$rules = [
				'video' => 'required',
				'user_id' => 'required',
				'title' => 'required',
				'author' => '',
				'description' => '',
				'who_see' => 'required',
				'age' => 'required',
				'find_by' => 'required'
			];
			$customeMessage = [
				'video.required' => 'Please send video.',
				'user_id.required' => 'Please enter user id.',
				'title.required' => 'Please enter title.',
				'author.required' => 'Please enter author name.',
				'who_see.required' => 'Please enter who can see.',
				'age.required' => 'Please enter age.',
				'find_by.required' => 'Please enter find by.'
			];
			$validator = Validator::make($request->all(),$rules,$customeMessage);
	        if($validator->fails()){
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        }else{
	        	$video = trim($request->video);
	        	$user_id = trim($request->user_id);
	        	$title = trim($request->title);
	        	$author = trim($request->author);
	        	$description = trim($request->description);
	        	$who_see = trim($request->who_see);
	        	$age = trim($request->age);
	        	$find_by = trim($request->find_by);
                $shareable_id = UserHelper::getUserShareable($user_id);

	        	if($request->hasFile('video'))
                {
                	$file = $request->video;
                	$mime = $file->getMimeType();
                	if($mime == "video/x-flv" || $mime == "video/mp4" || $mime == "video/3gpp" || $mime == "video/quicktime" || $mime == "video/x-msvideo" || $mime == "video/x-ms-wmv" || $mime == "video/webm" || $mime == "video/ogg" || $mime == "video/x-m4v" )
                	{
	                	if($shareable_id==="UserNotFound"){
	                		ResponseMessage::error('User not found.');
	                	}else{
		                    $file = $request->video;
		                    $time = md5(time());
		                    $video_name = $time.'.'.$file->getClientOriginalExtension();
	                		$path = 'user/'.$shareable_id.'/videos/'.$video_name;
		                    $file->move(public_path('user/'.$shareable_id.'/videos/'),$video_name);

	                		if(!File::exists(public_path('user/'.$shareable_id.'/videos/thumbnail/'))) {
							    File::makeDirectory(public_path('user/'.$shareable_id.'/videos/thumbnail/'), 0777, true, true);
							}
		                    $video_path = public_path($path);
	                		$thumbnail_image  = $time.".png";
	                		$thumbnail_path = public_path('user/'.$shareable_id.'/videos/thumbnail/').$thumbnail_image;
	                		$thumb_path = 'user/'.$shareable_id.'/videos/thumbnail/'.$thumbnail_image;;

	                		$temp = shell_exec("ffmpeg -i $video_path -deinterlace -an -ss 1 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $thumbnail_path 2>&1");

		                    $video = New Video;
		                    $video->user_id = $user_id;
		                    $video->title = $title;
		                    $video->author = $author;
		                    $video->description = $description;
		                    $video->path = $path;
		                    $video->thumb_path = $thumb_path;
		                    $video->who_see = $who_see;
		                    $video->age = $age;
		                    $video->find_by = $find_by;
		                    if($video->save()){
		                    	ResponseMessage::success('Video successfully upload.',$video);
		                    }else{
		                    	ResponseMessage::error('Somwthig was wrong in video upload please try again.');	
		                    }

	                	}
                	}		
            		else
            		{
                		ResponseMessage::error('Invalid video formate.');
            		}
                }else{
                	ResponseMessage::error('Somwthig was wrong in video upload please try again.');	
                }
	        }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function get_all_videos()
    {
    	try {
			$videoList = Video::leftjoin('users','users.id','=','videos.user_id')
					->select('videos.*','users.name as upload_by','users.profile')
					->orderBy('videos.id','DESC')
					->get();
			ResponseMessage::success('Video List.',$videoList);
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function delete_video($id)
    {
    	try {
    		if(Video::where('id',$id)->exists()){
    			$videoInfo = Video::find($id)->delete();
				ResponseMessage::success('Video successfully delete.',$videoInfo);
    		}else{
    			ResponseMessage::error('Video is not exists please try again.');	
    		}
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }
}
