<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Exception extends Model
{
    protected $table = "exceptions";
	public $primaryKey = 'id'; 
}
