<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
	protected $table = "favourites";
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = null;
}
