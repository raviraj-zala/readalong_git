<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
	use SoftDeletes;
    protected $table = "videos";
    public $primaryKey = "id";
    protected $dates = ['deleted_at'];
}
