<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Views extends Model
{
    protected $table = "views";
    public $primaryKey = "id";
    public $timestamps = null;
}
