<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// API VERSION 1
Route::prefix('v1')->group(function () {
	Route::post('signup','RESTAPIs\v1\UserserviceController@signup');
	Route::post('login','RESTAPIs\v1\UserserviceController@login');
	Route::post('upload_video','RESTAPIs\v1\VideoserviceController@upload_video');
	Route::get('delete_video/{id}','RESTAPIs\v1\VideoserviceController@delete_video');
	Route::post('get_all_videos','RESTAPIs\v1\VideoserviceController@get_all_videos');

	Route::get('get_category','RESTAPIs\v1\CategoryserviceController@get_category');
	Route::get('get_learningprofile','RESTAPIs\v1\LearningprofileserviceController@get_learningprofile');

	Route::post('add_favourite','RESTAPIs\v1\VideoserviceController@add_favourite');	

	Route::post('get_single_video','RESTAPIs\v1\VideoserviceController@get_single_video');
});
