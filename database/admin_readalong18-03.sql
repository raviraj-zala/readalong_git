-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2020 at 07:29 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_readalong`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=inactive,1=active',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `name`, `status`, `created_at`) VALUES
(1, 'Crafts', '0', '2018-10-10 00:00:00'),
(2, 'Outdoors', '0', '2018-10-10 00:00:00'),
(4, 'Music', '0', '2018-10-10 00:00:00'),
(5, 'Cars', '0', '2018-10-10 00:00:00'),
(6, 'Animals', '0', '2018-10-10 00:00:00'),
(7, 'Shapes', '0', '2018-10-10 00:00:00'),
(8, 'Books', '0', '2018-10-10 00:00:00'),
(9, 'People', '0', '2018-10-10 00:00:00'),
(10, 'Babies', '0', '2018-10-10 00:00:00'),
(11, 'Cooking', '0', '2018-10-10 00:00:00'),
(12, 'Dancing', '0', '2018-10-10 00:00:00'),
(13, 'Building', '1', '2018-11-12 00:00:00'),
(14, 'Making ', '1', '2018-11-12 00:00:00'),
(15, 'Reading', '1', '2018-11-12 00:00:00'),
(16, 'Singing', '1', '2018-11-12 00:00:00'),
(17, 'Drawing', '1', '2018-11-12 00:00:00'),
(18, 'Moving', '1', '2018-11-12 00:00:00'),
(19, 'Crafting', '1', '2018-11-12 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `exceptions`
--

CREATE TABLE `exceptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exception` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `user_id` bigint(20) NOT NULL,
  `video_id` varchar(20) NOT NULL,
  `is_favourite` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `learningprofiles`
--

CREATE TABLE `learningprofiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `learningprofiles`
--

INSERT INTO `learningprofiles` (`id`, `name`, `status`, `created_at`) VALUES
(1, 'Speaking', '1', '2018-10-10 00:00:00'),
(2, 'Potty training', '1', '2018-10-10 00:00:00'),
(3, 'Reading', '1', '2018-10-10 00:00:00'),
(4, 'Sharing', '1', '2018-10-10 00:00:00'),
(5, 'Getting dressed', '1', '2018-10-10 00:00:00'),
(6, 'Brushing teeth', '1', '2018-10-10 00:00:00'),
(7, 'Writing', '1', '2018-10-10 00:00:00'),
(8, 'Sleeping', '1', '2018-10-10 00:00:00'),
(9, 'Eating', '1', '2018-10-10 00:00:00'),
(10, 'Exercising', '1', '2018-10-10 00:00:00'),
(11, 'Counting', '1', '2018-10-10 00:00:00'),
(12, 'Gardening', '1', '2018-10-10 00:00:00'),
(13, 'Cleaning', '1', '2018-10-10 00:00:00'),
(14, 'Hand Coordination', '1', '2018-11-12 00:00:00'),
(15, 'Just For Fun', '1', '2018-11-12 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(15, '2014_10_12_000000_create_users_table', 1),
(16, '2014_10_12_100000_create_password_resets_table', 1),
(17, '2018_09_22_132244_add_column_user_table', 1),
(18, '2018_09_22_133920_add_reg_type_cloum_user_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `shareable_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_type` enum('Android','Iphone','Other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_token` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb_app_id` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gmail_app_id` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0 = in-active , 1 = active',
  `register_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `shareable_id`, `name`, `email`, `mobile_no`, `username`, `password`, `profile`, `device_type`, `device_token`, `fb_app_id`, `gmail_app_id`, `status`, `register_type`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'LVKUp3S5R1', '', 'k1@gmail.com', '', 'Kkkkkk', '$2y$10$98SEcZhE0DI2BloAsSU/jutQEk32wOGjXDHRKWwl7QNJNgzkysLNm', 'user/LVKUp3S5R1/profile/1200701e4374264e19228607650b5976.jpg', 'Iphone', 'harsh2810@H', '', '', '1', 'Basic', NULL, '2018-10-04 13:17:29', '2018-10-04 14:12:13', NULL),
(2, '6AieGFhprL', '', 'gal@gal.com', '', 'Galious', '$2y$10$1Gd4N7eCDu4bkpwxUAj9UObfKpED9I3sv4lrmVIs0U3fdiPlP4qMq', 'user/6AieGFhprL/profile/069d4dad5c308cc6fda571c59f898416.jpg', 'Iphone', '', '', '', '1', 'Basic', NULL, '2018-10-04 18:32:05', '2018-10-04 18:32:05', NULL),
(3, 'BlkZC0tVfS', '', 'gal@readalong.com', '', 'ReadAlong', '$2y$10$bpu9dBz.NGZrOwMzE032zO3g/yYvnnDcXAG2gJpZBLdNvERAE68Fq', 'user/BlkZC0tVfS/profile/b6038ca9dc803cd96e55d5d2b79d4577.jpg', 'Iphone', '', '', '', '1', 'Basic', NULL, '2018-10-08 00:45:56', '2018-10-08 00:45:56', NULL),
(4, 'R9joSgLjNl', '', 'shineinfosoft24@gmail.com', '', 'Ravi21', '$2y$10$m/mKfX4YVuAiMpuMGjVqfugoikvPhW55TAYyJn8lv30DgZX8rXgFG', 'user/R9joSgLjNl/profile/736f76ff93138882223ee06ec796588e.jpg', 'Iphone', '', '', '', '1', 'Basic', NULL, '2018-10-11 12:10:57', '2018-10-11 12:10:57', NULL),
(5, 'Ow4UQlHlkP', '', 'gal@galious.com', '', 'Galious', '$2y$10$0WGJffRSLu.eq7hM85Ykz.fcVGmzwuFKOnG/hcp/LVbpAEfrEOAqy', 'user/Ow4UQlHlkP/profile/223ce10973fb8770166fc03a7aaaf271.jpg', 'Iphone', '', '', '', '1', 'Basic', NULL, '2018-11-05 06:34:03', '2018-11-05 06:34:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb_path` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `learning_profile` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `find_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `user_id`, `unique_id`, `title`, `description`, `path`, `thumb_path`, `duration`, `learning_profile`, `age`, `find_by`, `file_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(10, 4, 'POST1542198716', 'video title', 'video description', 'user/R9joSgLjNl/images/075b15e4b3fa9c83e74093c75d4e1b913.jpg', '', '', 'sdgsdfg', '18-30', 'Colors', 'image', '2018-11-14 07:01:56', '2018-11-14 07:01:56', NULL),
(12, 4, 'POST1542198754', 'video title', 'video description', 'user/R9joSgLjNl/images/06c4b687e230bcc31d74004441753c153.jpg', '', '', 'sdgsdfg', '18-30', 'Colors', 'image', '2018-11-14 07:02:34', '2018-11-14 07:02:34', NULL),
(13, 4, 'POST1542198754', 'video title', 'video description', 'user/R9joSgLjNl/images/16c4b687e230bcc31d74004441753c153.jpg', '', '', 'sdgsdfg', '18-30', 'Colors', 'image', '2018-11-14 07:02:34', '2018-11-14 07:02:34', NULL),
(14, 4, 'POST1542198770', 'video title', 'video description', 'user/R9joSgLjNl/images/0d4f4445bc5b551cc108110f102cda561.jpg', '', '', 'sdgsdfg', '18-30', 'Colors', 'image', '2018-11-14 07:02:50', '2018-11-14 07:02:50', NULL),
(15, 4, 'POST1542198770', 'video title', 'video description', 'user/R9joSgLjNl/images/1d4f4445bc5b551cc108110f102cda561.jpg', '', '', 'sdgsdfg', '18-30', 'Colors', 'image', '2018-11-14 07:02:50', '2018-11-14 07:02:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `video_unique_id` varchar(20) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exceptions`
--
ALTER TABLE `exceptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `learningprofiles`
--
ALTER TABLE `learningprofiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `exceptions`
--
ALTER TABLE `exceptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `learningprofiles`
--
ALTER TABLE `learningprofiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
