<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',190);
            $table->string('email',190)->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('profile',190);
            $table->enum('device_type',['Android', 'Iphone', 'Other']);
            $table->string('device_token',190)->nullable();
            $table->string('fb_app_id',190)->nullable();
            $table->string('gmail_app_id',190)->nullable();
            $table->enum('status',['0','1'])->default('1')->comment('0 = in-active , 1 = active');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
